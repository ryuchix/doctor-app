<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBloodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bloods', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client_id')->nullable();
            $table->string('user_id')->nullable();
            $table->string('date')->nullable();
            $table->string('cr')->nullable();
            $table->string('hb1ac')->nullable();
            $table->string('fbgl')->nullable();
            $table->string('tc')->nullable();
            $table->string('tg')->nullable();
            $table->string('ldl')->nullable();
            $table->string('hdl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bloods');
    }
}
