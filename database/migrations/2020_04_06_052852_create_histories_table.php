<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client_id')->nullable();
            $table->string('smoker_status')->nullable();
            $table->string('years_smoke_quiting')->nullable();
            $table->string('years_smoked')->nullable();
            $table->string('cigar_per_day')->nullable();
            $table->string('diabtes_type')->nullable();
            $table->string('hba1c')->nullable();
            $table->string('fasting_bsl')->nullable();
            $table->string('cretinine')->nullable();
            $table->string('hypertension_type')->nullable();
            $table->string('dyslipidaemia_type')->nullable();
            $table->string('medical_management')->nullable();
            $table->string('pci')->nullable();
            $table->string('pci_years')->nullable();
            $table->string('cabg')->nullable();
            $table->string('cabg_years')->nullable();
            $table->string('aortic_valve_regurgitation')->nullable();
            $table->string('aortic_valve_stenosis')->nullable();
            $table->string('mitral_valve_regurgitation')->nullable();
            $table->string('mitral_valve_stenosis')->nullable();
            $table->string('pulmonary_valve_regurgitation')->nullable();
            $table->string('pulmonary_valve_stenosis')->nullable();
            $table->string('tricuspid_valve_regurgitation')->nullable();
            $table->string('tricuspid_valve_stenosis')->nullable();
            $table->string('severity')->nullable();
            $table->string('pacemaker_type')->nullable();
            $table->string('manufacturer')->nullable();
            $table->string('date_implanted')->nullable();
            $table->string('date_checked')->nullable();
            $table->string('icd_type')->nullable();
            $table->string('icd_manufacturer')->nullable();
            $table->string('icd_date_implanted')->nullable();
            $table->string('icd_date_checked')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
