<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEstsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('est', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('empty')->default(0);
            $table->string('stress_test')->nullable();
            $table->string('stress_test_min')->nullable();
            $table->string('stress_test_sec')->nullable();
            $table->string('stress_test_stage')->nullable();
            $table->string('stress_test_peak_pr')->nullable();
            $table->string('stress_test_peak_pr_max')->nullable();
            $table->string('stress_test_peak_bp')->nullable();
            $table->string('change_pain')->nullable();
            $table->string('dyspnoea')->nullable();
            $table->string('st_depression')->nullable();
            $table->string('st_depression_anterior')->nullable();
            $table->string('st_depression_anterior_size')->nullable();
            $table->string('st_depression_anterior_options')->nullable();
            $table->string('st_depression_lateral')->nullable();
            $table->string('st_depression_lateral_size')->nullable();
            $table->string('st_depression_lateral_options')->nullable();
            $table->string('st_depression_inferior')->nullable();
            $table->string('st_depression_inferior_size')->nullable();
            $table->string('st_depression_inferior_options')->nullable();
            $table->string('st_depression_inferolateral')->nullable();
            $table->string('st_depression_inferolateral_size')->nullable();
            $table->string('twave_change')->nullable();
            $table->string('twave_change_anterior')->nullable();
            $table->string('twave_change_anterior_options')->nullable();
            $table->string('twave_change_lateral')->nullable();
            $table->string('twave_change_lateral_options')->nullable();
            $table->string('twave_change_inferior')->nullable();
            $table->string('twave_change_inferior_options')->nullable();
            $table->string('twave_change_inferolateral')->nullable();
            $table->string('twave_change_inferolateral_options')->nullable();
            $table->string('hypokinesis')->nullable();
            $table->string('hypokinesis_segment')->nullable();
            $table->string('hypotension')->nullable();
            $table->string('hypotension_up')->nullable();
            $table->string('hypotension_down')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('est');
    }
}
