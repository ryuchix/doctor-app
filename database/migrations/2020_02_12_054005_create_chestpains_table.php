<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChestpainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chestpain', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('presenting_complaint_id')->nullable();
            $table->string('complaint')->nullable();
            $table->string('chestpain_history')->nullable();
            $table->string('chestpain_position')->nullable();
            $table->string('chestpain_status')->nullable();
            $table->string('chestpain_onset')->nullable();
            $table->string('chestpain_duration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chestpain');
    }
}
