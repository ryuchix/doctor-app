<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChestPainColumninPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patients', function (Blueprint $table) {

            $table->string('rhythm')->nullable()->after('pulse_rate');

            $table->string('complex')->nullable();
            $table->string('complex_location')->nullable();
            $table->string('complex_bundle')->nullable();

            $table->string('axis')->nullable();
            $table->string('left_anterior_hemiblock')->nullable();

            $table->string('qwaves')->nullable();
            $table->string('qwaves_location')->nullable();

            $table->string('twave_flattening')->nullable();
            $table->string('twave_flattening_location')->nullable();
            $table->string('twave_inversion')->nullable();
            $table->string('twave_inversion_location')->nullable();

            $table->string('st_depression')->nullable();
            $table->string('st_depression_location')->nullable();
            $table->string('st_depression_height')->nullable();
            $table->string('st_elevation')->nullable();
            $table->string('st_elevation_location')->nullable();
            $table->string('st_elevation_height')->nullable();

            $table->string('heartblock')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
