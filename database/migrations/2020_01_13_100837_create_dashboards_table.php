<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDashboardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('title')->nullable();
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('age')->nullable();
            $table->string('sex')->nullable();
            $table->string('smoker')->nullable();
            $table->string('healthcare')->nullable();
            $table->string('account_no')->nullable();
            $table->string('referring_name')->nullable();
            $table->string('referring_provider_no')->nullable();
            $table->string('referring_address')->nullable();


            $table->string('fbg')->nullable();
            $table->string('cr')->nullable();
            $table->string('hba1c')->nullable();
            $table->string('tc')->nullable();
            $table->string('tg')->nullable();   
            

            $table->string('bp_over')->nullable();
            $table->string('bp_under')->nullable();
            $table->string('pr_over')->nullable();
            $table->string('pr_under')->nullable();
            $table->integer('exertional_')->nullable()->default(0);
            $table->string('exertional_bp_over')->nullable();
            $table->string('exertional_bp_under')->nullable();
            $table->string('exertional_pr_over')->nullable();
            $table->string('exertional_pr_under')->nullable();
            $table->string('wheeze')->nullable();
            $table->string('crackles')->nullable();
            $table->string('ronchi')->nullable();
            $table->string('reduced_air_entry')->nullable();
            $table->text('message')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
