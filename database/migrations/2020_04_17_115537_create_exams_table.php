<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('client_id')->nullable();
            $table->string('bp_over')->nullable();
            $table->string('bp_under')->nullable();
            $table->string('pr_over')->nullable();
            $table->string('pr_under')->nullable();
            $table->string('exertional_bp_over')->nullable();
            $table->string('exertional_bp_under')->nullable();
            $table->string('exertional_pr_over')->nullable();
            $table->string('exertional_pr_under')->nullable();
            $table->string('cardiac_exam')->nullable();
            $table->string('ejection_murmur_options')->nullable();
            $table->string('lungs_image')->nullable();
            $table->string('lungs_box')->nullable();
            $table->string('head_neck')->nullable();
            $table->string('abdomen')->nullable();
            $table->string('peripheral_oedema')->nullable();
            $table->string('peripheral_oedema_option1')->nullable();
            $table->string('peripheral_oedema_option2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exams');
    }
}