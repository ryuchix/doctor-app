<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeUserIdInTteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tte', function (Blueprint $table) {
            $table->renameColumn('user_id', 'client_id');
        });

        Schema::table('est', function (Blueprint $table) {
            $table->renameColumn('user_id', 'client_id');
        });

        Schema::table('hx', function (Blueprint $table) {
            $table->renameColumn('user_id', 'client_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tte', function (Blueprint $table) {
            //
        });
    }
}
