<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hx', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('empty')->default(0);
            $table->string('chestpain')->nullable();
            $table->string('soboe')->nullable();
            $table->string('orthopnoea')->nullable();
            $table->string('pnd')->nullable();
            $table->string('palpitations')->nullable();
            $table->string('presyncope')->nullable();
            $table->string('syncope')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hx');
    }
}
