<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewColumnForTteInPatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tte', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('empty')->default(0);
            $table->string('lv_thickness')->nullable();
            $table->string('lvidd')->nullable();
            $table->string('lvef')->nullable();
            $table->string('la_size')->nullable();
            $table->string('ra_size')->nullable();
            $table->string('aortic_root')->nullable();
            $table->string('ascending_aorta')->nullable();
            $table->string('intracadiac_shunts')->nullable();
            $table->string('aortic_valve')->nullable();
            $table->string('aortic_valve_sclerosis')->nullable();
            $table->string('aortic_valve_sclerosis_mean_gradient')->nullable();
            $table->string('aortic_valve_regurgitation')->nullable();
            $table->string('aortic_valve_regurgitation_pht')->nullable();
            $table->string('bicuspid_aortic_valve')->nullable();
            $table->string('mintral_valve')->nullable();
            $table->string('mintral_valve_stenosis')->nullable();
            $table->string('mintral_valve_stenosis_mean_gradient')->nullable();
            $table->string('mintral_valve_regurgitation')->nullable();
            $table->string('mintral_valve_regurgitation_condition')->nullable();
            $table->string('mintral_valve_regurgitation_prolapse')->nullable();

            $table->string('tricuspid_valve_regurgitation')->nullable();
            $table->string('tricuspid_valve_regurgitation_condition')->nullable();
            $table->string('tricuspid_valve_regurgitation_pasp')->nullable();

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tte');
    }
}
