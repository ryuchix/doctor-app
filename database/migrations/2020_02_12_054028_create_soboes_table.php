<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSoboesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soboe', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('presenting_complaint_id')->nullable();
            $table->string('complaint')->nullable();
            $table->string('soboe_exertion')->nullable();
            $table->string('soboe_condition')->nullable();
            $table->string('soboe_onset')->nullable();
            $table->string('soboe_duration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soboe');
    }
}
