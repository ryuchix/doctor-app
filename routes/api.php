<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register', 'API\AuthController@register');
Route::post('login', 'API\AuthController@login');

Route::group(['middleware' => 'jwt.refresh'], function(){
	Route::get('refresh', 'API\AuthController@refresh');
});

Route::group(['middleware' => 'jwt.auth'], function(){
	Route::get('user', 'API\AuthController@user');
	Route::post('user', 'API\AuthController@updateUser');
	Route::post('logout', 'API\AuthController@logout');
	Route::post('change-password', 'API\AuthController@changePassword');

	Route::get('generate-letter/{id}', 'API\DashboardController@generateLetter');

	Route::get('get-clients', 'API\DashboardController@getClients');
	Route::get('get-client/{id}', 'API\DashboardController@getClient');
	Route::post('update-client/{id}', 'API\DashboardController@updateClient');
	Route::post('add-client', 'API\DashboardController@addClient');


	Route::post('save-profile/{id}', 'API\DashboardController@savePatientData');
	Route::get('get-profile/{id}', 'API\DashboardController@getPatientData');

	Route::post('save-tte/{id}', 'API\DashboardController@saveTTEData');
	Route::get('get-tte/{id}', 'API\DashboardController@getTTEData');

	Route::post('save-est/{id}', 'API\DashboardController@saveESTData');
	Route::get('get-est/{id}', 'API\DashboardController@getESTData');

	Route::post('save-hx/{id}', 'API\DashboardController@saveHxData');
	Route::get('get-hx/{id}', 'API\DashboardController@getHxData');

	Route::post('save-letter/{id}', 'API\DashboardController@saveLetter');

	Route::post('save-phx/{id}', 'API\HistoryController@saveHistory');
	Route::get('get-phx/{id}', 'API\HistoryController@getHistory');

	Route::get('get-letters', 'API\DashboardController@getLetters');

	Route::post('save-blood/{id}', 'API\BloodController@saveBlood');
	Route::post('update-blood/{id}', 'API\BloodController@updateBlood');
	Route::get('get-bloods/{id}', 'API\BloodController@getBloods');
	Route::get('get-blood/{id}', 'API\BloodController@getBlood');
	Route::get('delete-blood/{id}', 'API\BloodController@deleteBlood');
	Route::get('first-blood/{id}', 'API\BloodController@getFirstBlood');


	Route::post('save-exam/{id}', 'API\ExamController@saveExam');

	Route::get('delete-account/{id}', 'API\AuthController@deleteAccount');
	

});

Route::get('get-letter/{id}', 'API\DashboardController@getLetter');

// Route::post('save-phx', 'API\DashboardController@savePHXData');
// Route::get('get-phx', 'API\DashboardController@getPHXData');

// Route::post('save-exam', 'API\DashboardController@saveExamData');
// Route::get('get-exam', 'API\DashboardController@getExamData');



Route::get('get-doctors', 'API\DoctorController@getDoctors');
Route::get('get-doctor/{id}', 'API\DoctorController@getDoctor');
Route::post('add-doctor', 'API\DoctorController@addDoctor');
Route::post('update-doctor/{id}', 'API\DoctorController@updateDoctor');
Route::get('delete-doctor/{id}', 'API\DoctorController@deleteDoctor');

Route::get('get-practices', 'API\PracticeController@getPractices');
Route::get('get-practice/{id}', 'API\PracticeController@getPractice');
Route::post('add-practice', 'API\PracticeController@addPractice');
Route::post('update-practice/{id}', 'API\PracticeController@updatePractice');
Route::get('delete-practice/{id}', 'API\PracticeController@deletePractice');
Route::get('pick-practice/{practiceid}/{patientid}', 'API\PracticeController@pickPractice');

Route::get('meds', 'API\MedController@getMeds');
Route::get('med/{id}', 'API\MedController@getMed');
Route::post('add-med', 'API\MedController@addMed');
Route::post('update-med/{id}', 'API\MedController@updateMed');
Route::get('delete-med/{id}', 'API\MedController@deleteMed');


