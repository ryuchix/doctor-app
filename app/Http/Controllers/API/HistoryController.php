<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\History;
use App\Models\Patient;
use App\Models\Client;

class HistoryController extends Controller
{
    public function saveHistory(Request $request, $id) {
    	$inputs = $request->all();
    	$inputs['client_id'] = $id;
        $inputs['history'] = !$request->has('history') ? NULL : $request->history;

        $history = History::where('client_id', $id)->first();

        if ($history == null) {
        	History::updateOrCreate(['client_id' => $id], [$inputs]);
        } else {
        	History::updateOrCreate(['client_id' => $id], $inputs);
        }

    }

    public function getHistory($id) {
    	return History::where('client_id', $id)->first();
    }
}
