<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Patient;
use App\Models\TTE;
use App\Models\Est;
use App\Models\Hx;
use App\Models\Chestpain;
use App\Models\Soboe;
use App\User;
use PDF;
use App\Models\Letter;
use App\Models\Doctor;
use App\Models\Practice;
use App\Models\Client;
use App\Models\Exam;
use App\Models\History;
use Auth;

class DashboardController extends Controller
{

    public function generateLetter($id) {
        $client = Client::find($id);
        $patient = new Patient();
        $patient->client_id = $id;
        $patient->title = $client->title;
        $patient->firstname = $client->firstname;
        $patient->lastname = $client->lastname;
        $patient->age = $client->age;
        $patient->medicare = $client->medicare;
        $patient->sex = $client->sex;
        $patient->save();
        $patient->save();

        return response([
            'status' => 'success',
            'data' => $patient->id
        ]);
    }

    public function savePatientData(Request $request, $id) {
        $inputs = $request->all();
        $inputs['exertional_'] = $request->exertional_bp_over != null ? 1 : 0;
        $inputs['wheeze'] = $request->wheeze != null ? 'wheeze' : NULL;
        $inputs['crackles'] = $request->crackles != null ? 'crackles' : NULL;
        $inputs['ronchi'] = $request->ronchi != null ? 'ronchi' : NULL;
        $inputs['reduced_air_entry'] = $request->reduced_air_entry != null ? 'reduced air entry' : NULL;

        $inputs['complex_location'] = $request->complex == 'narrow complexes' ? NULL : $request->complex_location;
        $inputs['complex_bundle'] = $request->complex_bundle != null ? $request->complex_bundle : NULL;
        $inputs['left_anterior_hemiblock'] = $request->axis != 'left axis deviation' ? NULL : $request->left_anterior_hemiblock;
        $inputs['qwaves'] = $request->qwaves != null ? $request->qwaves : NULL;
        $inputs['qwaves_location'] = $request->qwaves == null ? NULL : $request->qwaves_location;

        $inputs['twave_flattening'] = $request->twave_flattening != null ? $request->twave_flattening : NULL;
        $inputs['twave_flattening_location'] = $request->twave_flattening == null ? NULL : $request->twave_flattening_location;
        $inputs['twave_inversion'] = $request->twave_inversion != null ? $request->twave_inversion : NULL;
        $inputs['twave_inversion_location'] = $request->twave_inversion == null ? NULL : $request->twave_inversion_location;

        $inputs['st_depression'] = $request->st_depression != null ? $request->st_depression : NULL;
        $inputs['st_depression_location'] = $request->st_depression == null ? NULL : $request->st_depression_location;
        $inputs['st_depression_height'] = $request->st_depression == null ? NULL : $request->st_depression_height;
        $inputs['st_elevation'] = $request->st_elevation != null ? $request->st_elevation : NULL;
        $inputs['st_elevation_location'] = $request->st_elevation == null ? NULL : $request->st_elevation_location;
        $inputs['st_elevation_height'] = $request->st_elevation == null ? NULL : $request->st_elevation_height;

        $clients = $request->all();

    	$patient = Patient::updateOrCreate(['id' => $id], $inputs);

        return $patient;
    }

    public function getPatientData($id) {
    	$patient = Patient::find($id);
        $doctor = Doctor::find($patient->doctor_id);

        $practice = Practice::find($patient->practice_id);
        $client = Client::find($patient->client_id);
        $exam = Exam::where('client_id', $id)->first();
        $phx = History::where('client_id', $id)->first();
        $est = Est::where('client_id', $id)->first();

        $patient['referral_doctor'] = $doctor;
        $patient['practice'] = $practice;
        $patient['basic_info'] = $client;
        $patient['exam'] = $exam;
        $patient['phx'] = $phx;
        $patient['est'] = $est;


        return $patient;
    }

    public function saveTTEData(Request $request, $id) {
        $inputs = $request->all();
        $inputs['empty'] = 1;
        $inputs['intracadiac_shunts'] = $request->intracadiac_shunts == NULL ? NULL : $request->intracadiac_shunts;
        $inputs['bicuspid_aortic_valve'] = $request->bicuspid_aortic_valve == NULL ? NULL : $request->bicuspid_aortic_valve;
        $inputs['mintral_valve_regurgitation_prolapse'] = $request->mintral_valve_regurgitation_prolapse == NULL ? NULL : $request->mintral_valve_regurgitation_prolapse;
        $patient = TTE::updateOrCreate(['client_id' => $id], $inputs);
        //$this->user->sendPusher($patient, 'patient-channel', 'patient-event');
        
        return $patient;
    }

    public function getTTEData($id) {
        return TTE::where('client_id', $id)->first();
    }

    public function saveESTData(Request $request, $id) {
        $inputs = $request->all();
        $inputs['change_pain'] = $request->change_pain == NULL ? NULL : $request->change_pain;
        $inputs['dyspnoea'] = $request->dyspnoea == NULL ? NULL : $request->dyspnoea;

        $inputs['st_depression'] = $request->st_depression == NULL ? NULL : $request->st_depression;
        $inputs['twave_change'] = $request->twave_change == NULL ? NULL : $request->twave_change;
        $inputs['hypokinesis'] = $request->hypokinesis == NULL ? NULL : $request->hypokinesis;
        $inputs['hypotension'] = $request->hypotension == NULL ? NULL : $request->hypotension;

        $inputs['st_depression_anterior_size'] = $request->st_depression_anterior_size == NULL ? NULL : $request->st_depression_anterior_size;
        $inputs['st_depression_lateral_size'] = $request->st_depression_lateral_size == NULL ? NULL : $request->st_depression_lateral_size;
        $inputs['st_depression_inferior_size'] = $request->st_depression_inferior_size == NULL ? NULL : $request->st_depression_inferior_size;
        $inputs['st_depression_inferolateral_size'] = $request->st_depression_inferolateral_size == NULL ? NULL : $request->st_depression_inferolateral_size;

        $inputs['st_depression_anterior'] = $request->st_depression == NULL ? NULL : $request->st_depression_anterior;
        $inputs['st_depression_anterior_size']  = $request->st_depression == NULL ? NULL : $request->st_depression_anterior_size;
        $inputs['st_depression_anterior_options']  = $request->st_depression == NULL ? NULL : $request->st_depression_anterior_options;
        $inputs['st_depression_lateral']  = $request->st_depression == NULL ? NULL : $request->st_depression_lateral;
        $inputs['st_depression_lateral_size']  = $request->st_depression == NULL ? NULL : $request->st_depression_lateral_size;
        $inputs['st_depression_lateral_options'] = $request->st_depression == NULL ? NULL : $request->st_depression_lateral_options;
        $inputs['st_depression_inferior']  = $request->st_depression == NULL ? NULL : $request->st_depression_inferior;
        $inputs['st_depression_inferior_size']  = $request->st_depression == NULL ? NULL : $request->st_depression_inferior_size;
        $inputs['st_depression_inferior_options']  = $request->st_depression == NULL ? NULL : $request->st_depression_inferior_options;
        $inputs['st_depression_inferolateral']  = $request->st_depression == NULL ? NULL : $request->st_depression_inferolateral;
        $inputs['st_depression_inferolateral_size']  = $request->st_depression == NULL ? NULL : $request->st_depression_inferolateral_size;

        $inputs['twave_change_anterior'] = $request->twave_change == NULL ? NULL : $request->twave_change_anterior;
        $inputs['twave_change_anterior_options']  = $request->twave_change == NULL ? NULL : $request->twave_change_anterior_options;
        $inputs['twave_change_lateral']  = $request->twave_change == NULL ? NULL : $request->twave_change_lateral;
        $inputs['twave_change_lateral_options']  = $request->twave_change == NULL ? NULL : $request->twave_change_lateral_options;
        $inputs['twave_change_inferior']  = $request->twave_change == NULL ? NULL : $request->twave_change_inferior;
        $inputs['twave_change_inferior_options']  = $request->twave_change == NULL ? NULL : $request->twave_change_inferior_options;
        $inputs['twave_change_inferolateral']  = $request->twave_change == NULL ? NULL : $request->twave_change_inferolateral;
        $inputs['twave_change_inferolateral_options']  = $request->twave_change == NULL ? NULL : $request->twave_change_inferolateral_options;

        $patient = Est::updateOrCreate(['client_id' => $id], $inputs);
        //$this->user->sendPusher($patient, 'patient-channel', 'patient-event');
        
        return $patient;
    }

    public function getESTData($id) {
        return Est::where('client_id', $id)->first();
    }

    public function saveHxData(Request $request, $id) {
        $inputs = $request->all();
       
        $patient = Hx::updateOrCreate(['client_id' => $id], $inputs);
        $chestpain = Chestpain::updateOrCreate(['presenting_complaint_id' => $patient->id], $inputs);
        $soboe = Soboe::updateOrCreate(['presenting_complaint_id' => $patient->id], $inputs);
        
        return response()->json([
            'hx' => $patient,
            'chestpain' => $chestpain,
            'soboe' => $soboe,
        ], 200);
    }

    public function getHxData($id) {
        $hx = Hx::where('client_id', $id)->first();
        $chestpain = Chestpain::where('presenting_complaint_id', $hx->id)->first();
        $soboe = Soboe::where('presenting_complaint_id', $hx->id)->first();

        return response()->json([
            'hx' => $hx,
            'chestpain' => $chestpain,
            'soboe' => $soboe,
        ], 200);
    }

    public function getLetter($id) {
        $data = Letter::where('client_id', $id)->first();
        $data['practice'] = Practice::find($data->practice_id);
        $pdf = PDF::loadView('pdf_view', $data);
        return $pdf->download('letter.pdf');
    }

    public function saveLetter(Request $request, $id) {
        $inputs = $request->all();
        $inputs['user_id'] = Auth::user()->id;
        $letter = Letter::updateOrCreate(['client_id' => $id], $inputs);
    }

    public function getClients() {
        return Client::where('user_id', Auth::user()->id)->get();
    }

    public function getClient($id) {
        return Client::find($id);
    }

    public function addClient(Request $request) {
        $inputs = $request->all();
        $inputs['user_id'] = Auth::user()->id;
        $client = Client::create($inputs);

        return response([
            'message' => 'Success'
        ]);

    }

    public function updateClient(Request $request, $id) {
        $inputs = $request->all();
        $client = Client::find($id);
        $client->update($inputs);

        return response([
            'message' => 'Success'
        ]);
    }

    public function getLetters() {
        $letters = Letter::where('user_id', Auth::user()->id)->get();

        $letters->each(function($letter) {
            $patient = Patient::where('id', $letter->client_id)->first();

            $client = Client::find($patient->client_id);
            $letter['client'] = $client; 
        });

        return $letters;

    }

}
