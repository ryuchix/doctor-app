<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctor;
use App\User;

class DoctorController extends Controller
{

	protected $userid;
	protected $user;

    public function __construct() {
    	$this->user = User::find(1);
        $this->userid = $this->user->id;
    }

    public function getDoctors() {
    	return Doctor::where('user_id', $this->userid)->get();
    }

    public function getDoctor($id) {
    	return Doctor::find($id);
    }

    public function addDoctor(Request $request) {
    	$inputs = $request->all();
        $inputs['user_id'] = $this->user->id;
    	Doctor::create($inputs);

        return response()->json([
            'message' => 'Doctor added'
        ], 200);
    }

    public function updateDoctor(Request $request, $id) {
    	$inputs = $request->all();
    	$doctor = Doctor::find($id);
    	$doctor->update($inputs);

        return response()->json([
            'message' => 'Doctor updated'
        ], 200);

    }

    public function deleteDoctor($id) {
    	$doctor = Doctor::find($id);
    	$doctor->delete();

        return response()->json([
            'message' => 'Doctor deleted'
        ], 200);

    }
}



