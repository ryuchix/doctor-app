<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Practice;
use App\Models\Patient;
use App\Models\Letter;
use App\User;
use Auth;

class PracticeController extends Controller
{

    public function getPractices() {
    	return Practice::where('user_id', Auth::user()->id)->get();
    }

    public function getPractice($id) {
    	return Practice::find($id);
    }

    public function addPractice(Request $request) {
    	$inputs = $request->all();
        $inputs['user_id'] = Auth::user()->id;
    	$practice = Practice::create($inputs);

        return response()->json([
            'message' => 'Practice added'
        ], 200);
    }

    public function updatePractice(Request $request, $id) {
    	$inputs = $request->all();
    	$practice = Practice::find($id);
    	$practice->update($inputs);

        return response()->json([
            'message' => 'Practice updated'
        ], 200);

    }

    public function deletePractice($id) {
    	$practice = Practice::find($id);
    	$practice->delete();

        return response()->json([
            'message' => 'Practice deleted'
        ], 200);

    }

    public function pickPractice($practiceid, $patientid) {
        $practice = Practice::find($practiceid);
        $patient = Patient::find($patientid);
        $letter = Letter::where('client_id', $patientid)->first();
        $letter->practice_id = $practice->id;
        $letter->save();
        
        $patient->practice_id = $practice->id;
        if ($patient->save()) {
            return $practice;
        }
    }
}
