<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Exam;

class ExamController extends Controller
{
    public function saveExam(Request $request, $id) {
    	$inputs = $request->all();
        $inputs['cardiac_exam'] = $request->cardiac_exam != null ? $request->cardiac_exam : NULL;
        $inputs['lungs_image'] = $request->lungs_image != '' ? $request->lungs_image : NULL;
        $inputs['lungs_box'] = $request->lungs_box != null ? $request->lungs_box : NULL;
        $inputs['head_neck'] = $request->head_neck != null ? $request->head_neck : NULL;
        $inputs['abdomen'] = $request->abdomen != null ? $request->abdomen : NULL;
        $inputs['peripheral_oedema'] = $request->peripheral_oedema != null ? $request->peripheral_oedema : NULL;

		$exam = Exam::updateOrCreate(
		    ['client_id' => $id],
		    $inputs
		); 

        return response()->json([
            'message' => 'Exam recorded'
        ], 200);

    }
}
