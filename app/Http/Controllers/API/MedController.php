<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Meds;

class MedController extends Controller
{
    public function getMeds() {
    	return Meds::all();
    }

    public function getMed($id) {
    	return Meds::find($id);
    }

    public function updateMed(Request $request, $id) {
    	$inputs = $request->all();
    	$med = Meds::find($id);
    	$med->update($inputs);

        return response()->json([
            'message' => 'Meds updated'
        ], 200);
    }

    public function addMed(Request $request) {
    	$inputs = $request->all();
    	Meds::create($inputs);

        return response()->json([
            'message' => 'Meds added'
        ], 200);
    }

    public function deleteMed($id) {
    	$med = Meds::find($id);
    	$med->delete();

        return response()->json([
            'message' => 'Meds deleted'
        ], 200);

    }
}