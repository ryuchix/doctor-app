<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Blood;
use App\Models\Client;
use App\Models\Patient;
use Auth;

class BloodController extends Controller
{
    public function saveBlood(Request $request, $id) {

    	$inputs = $request->all();
    	$patient = Patient::find($id);
    	$inputs['client_id'] = $patient->client_id;
    	$inputs['user_id'] = Auth::user()->id;

        Blood::create($inputs);

    }

    public function updateBlood(Request $request, $id) {
    	$inputs = $request->except(['bloodid']);
    	$patient = Patient::find($id);
    	$inputs['client_id'] = $patient->client_id;
    	$inputs['user_id'] = Auth::user()->id;

    	Blood::where('id', $request->bloodid)->update($inputs);

    }

    public function getBloods($id) {
    	$patient = Patient::find($id);
    	$bloods = Blood::where('client_id', $patient->client_id)->where('user_id', Auth::user()->id)->get();
    	
    	return $bloods;
    }

    public function getBlood($id) {
    	return Blood::find($id);
    }

    public function deleteBlood($id) {
    	$blood = Blood::find($id);
    	$blood->delete();
    }

    public function getFirstBlood($id) {
        $patient = Patient::find($id);
        return Blood::where('client_id', $patient->client_id)->where('user_id', Auth::user()->id)->first();
    }
}
