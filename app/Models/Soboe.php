<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Soboe extends Model
{
    protected $table = 'soboe';

    protected $fillable = ['user_id', 'presenting_complaint', 'soboe_exertion', 'soboe_condition', 'soboe_onset', 'soboe_duration'];

    public function hx() {
    	return $this->belongsTo('App\Models\Hx', 'presenting_complaint');
    }
}