<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TTE extends Model
{
    protected $table = 'tte';

    protected $fillable = [

    	'client_id',
        'empty',
    	'lv_thickness',
    	'lvidd',
    	'lvef',
    	'la_size',
    	'ra_size',
    	'aortic_root',
    	'ascending_aorta',
    	'intracadiac_shunts',
    	'aortic_valve',
    	'aortic_valve_sclerosis',
    	'aortic_valve_sclerosis_mean_gradient',
    	'aortic_valve_regurgitation',
    	'aortic_valve_regurgitation_pht',
    	'bicuspid_aortic_valve',
    	'mintral_valve',
    	'mintral_valve_stenosis',
    	'mintral_valve_stenosis_mean_gradient',
    	'mintral_valve_regurgitation',
    	'mintral_valve_regurgitation_condition',
    	'mintral_valve_regurgitation_prolapse',
    	'tricuspid_valve_regurgitation',
    	'tricuspid_valve_regurgitation_condition',
    	'tricuspid_valve_regurgitation_pasp',

    ];


}
