<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blood extends Model
{
	protected $table = 'bloods';

	protected $fillable = [
		'client_id',
		'user_id',
		'date',
		'cr',
		'hb1ac',
		'fbgl',
		'tc',
		'tg',
		'ldl',
		'hdl'
	];

	public function getUpdatedAtAttribute($value) {
	    return \Carbon\Carbon::parse($value)->format('d/m/Y');
	}
}