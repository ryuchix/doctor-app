<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Patient extends Model
{
    protected $fillable = [

    	'title',
    	'user_id',
        'client_id',
        'doctor_id',
        'practice_id',
    	'firstname',
    	'lastname',
    	'age',
    	'sex',
    	'smoker',
    	'medicare',
    	'account_no',
    	'referring_name',
    	'referring_provider_no',
    	'referring_address',

        'fbg',
        'cr',
        'hba1c',
        'tc',
        'tg',

        'bp_over',
        'bp_under',
        'pr_over',
        'pr_under',
        'exertional_',
        'exertional_bp_over',
        'exertional_bp_under',
        'exertional_pr_over',
        'exertional_pr_under',
        'wheeze',
        'crackles',
        'ronchi',
        'reduced_air_entry',
        'message',

        'pulse_rate',
        'rhythm',
        'complex',
        'complex_location',
        'complex_bundle',
        'axis',
        'left_anterior_hemiblock',
        'qwaves',
        'qwaves_location',
        'twave_flattening',
        'twave_flattening_location',
        'twave_inversion',
        'twave_inversion_location',
        'st_depression',
        'st_depression_location',
        'st_depression_height',
        'st_elevation',
        'st_elevation_location',
        'st_elevation_height',
        'heartblock',


        'tte'

    ];



}
