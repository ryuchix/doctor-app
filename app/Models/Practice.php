<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Practice extends Model
{
    protected $fillable = ['title', 'firstname', 'lastname', 'qualifications', 'specialty', 'address1', 'address2', 'phone', 'fax', 'user_id', 'city', 'state', 'postcode', 'provider'];
}