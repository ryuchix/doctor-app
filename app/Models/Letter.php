<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Letter extends Model
{
	public $timestamps = true;
	
    protected $fillable = ['user_id', 'practice_id', 'letters', 'client_id'];

    public function client() {
    	return $this->belongsTo('App\Models\Client', 'client_id');
    }
}
