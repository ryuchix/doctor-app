<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Est extends Model
{
	protected $table = 'est';

    protected $fillable = [
    	'user_id',
        'client_id',
		'empty',
    	'stress_test',
    	'stress_test_min',
    	'stress_test_sec',
    	'stress_test_stage',
    	'stress_test_peak_pr',
    	'stress_test_peak_pr_max',
    	'stress_test_peak_bp',
    	'change_pain',
        'dyspnoea',
    	'st_depression',
    	'st_depression_anterior',
    	'st_depression_anterior_size',
    	'st_depression_anterior_options',
    	'st_depression_lateral',
    	'st_depression_lateral_size',
    	'st_depression_lateral_options',
    	'st_depression_inferior',
    	'st_depression_inferior_size',
    	'st_depression_inferior_options',
    	'st_depression_inferolateral',
    	'st_depression_inferolateral_size',
    	'twave_change',
    	'twave_change_anterior',
    	'twave_change_anterior_options',
    	'twave_change_lateral',
    	'twave_change_lateral_options',
    	'twave_change_inferior',
    	'twave_change_inferior_options',
    	'twave_change_inferolateral',
    	'twave_change_inferolateral_options',
    	'hypokinesis',
    	'hypokinesis_segment',
    	'hypotension',
    	'hypotension_up',
    	'hypotension_down',
    ];
}