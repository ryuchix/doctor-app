<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
	protected $table = 'histories';
	
	protected $fillable = [
		'client_id',
		'history',
		'smoker_status',
		'years_smoke_quiting', 
		'years_smoked',
		'cigar_per_day',
		'diabtes_type',
		'hba1c',
		'fasting_bsl',
		'cretinine',
		'hypertension_type',
		'dyslipidaemia_type',
		'medical_management',
		'pci',
		'pci_years',
		'cabg',
		'cabg_years',
		'aortic_valve_regurgitation',
		'aortic_valve_stenosis',
		'mitral_valve_regurgitation',
		'mitral_valve_stenosis',
		'pulmonary_valve_regurgitation',
		'pulmonary_valve_stenosis',
		'tricuspid_valve_regurgitation',
		'tricuspid_valve_stenosis',
		'severity',
		'pacemaker_type',
		'manufacturer',
		'date_implanted',
		'date_checked',
		'icd_type',
		'icd_manufacturer',
		'icd_date_implanted',
		'icd_date_checked',
	];

	public function getUpdatedAtAttribute($value) {
	    return \Carbon\Carbon::parse($value)->format('d/m/Y');
	}
}