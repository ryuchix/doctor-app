<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chestpain extends Model
{
    protected $table = 'chestpain';

    protected $fillable = ['user_id', 'presenting_complaint', 'chestpain_history', 'chestpain_position', 'chestpain_status', 'chestpain_onset', 'chestpain_duration'];

    public function hx() {
    	return $this->belongsTo('App\Models\Hx', 'presenting_complaint');
    }
}