<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
	protected $table = 'exams';

    protected $fillable = [
        'client_id',
		'bp_over',
    	'bp_under',
    	'pr_over',
    	'pr_under',
    	'exertional_bp_over',
    	'exertional_bp_under',
    	'exertional_pr_over',
    	'exertional_pr_under',
        'cardiac_exam',
        'ejection_murmur_options',
        'lungs_image',
        'lungs_box',
        'head_neck',
        'abdomen',
        'peripheral_oedema',
        'peripheral_oedema_option1',
        'peripheral_oedema_option2',
    ];
}