<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    protected $fillable = ['title', 'firstname', 'lastname', 'practice', 'specialty', 'address1', 'address2', 'phone', 'fax', 'user_id'];
}