<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hx extends Model
{
    protected $table = 'hx';

    protected $fillable = ['user_id', 'empty', 'complaints'];

    public function chestpain() {
    	return $this->belongsTo('App\Models\Chestpain', 'presenting_complaint');
    }

    public function soboe() {
    	return $this->belongsTo('App\Models\Soboe', 'presenting_complaint');
    }

}
