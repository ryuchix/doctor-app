<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PHX extends Model
{
    protected $table = 'phxs';

    protected $fillable = [

    	'user_id',
    	'fbg',
    	'cr',
    	'hba1c',
    	'tc',
    	'tg',

    ];

}
