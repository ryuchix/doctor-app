<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Meds extends Model
{
    protected $fillable = ['name', 'type', 'class', 'dose', 'frequency'];
}