<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
 	<title></title>
 	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Heebo:300,400&display=swap">
	<style type="text/css">
		body {
			line-height: 100%;
			font-family: 'Heebo';
			font-style: 'normal';
			font-size: 17px;
		}
		.letter-hl {
			line-height: 100%;
			font-family: 'Heebo';
			font-style: 'normal';
			font-size: 17px;
			font-weight: bold;
			text-decoration: underline;
		}
		.pmh {
			line-height: 100%;
			font-family: 'Heebo';
			font-style: 'normal';
			margin-bottom: 10px;
		}
		.center {
			text-align: center
		}
		.name {
			line-height: 100%;
			font-family: 'Heebo';
			font-style: 'normal';
			font-size: 24px;
    		font-weight: bold;
		}
		.info {
			line-height: 100%;
			font-family: 'Heebo';
			font-style: 'normal';
			font-size: 16px;
		}
		p {
			line-height: 100%;
			font-family: 'Heebo';
			font-style: 'normal';
		}
		.letter-hl {
			line-height: 100% !important;
			font-family: 'Heebo' !important;
			font-style: 'normal' !important;
			font-size: 18px;
		    font-weight: bold;
		    text-decoration: underline;
		}
	</style>
</head>
<body>
  <div>
  <div class="center">
  	<div class="name">{{ $practice['title'] }}. {{ $practice['firstname'] }} {{ $practice['lastname'] }}</div>
  	<div class="info">{{ $practice['qualifications'] }}</div>
  	<div class="info">{{ $practice['specialty'] }}</div>
  	<div class="info">{{ $practice['address1'] }} {{ $practice['city'] }} {{ $practice['state'] }} {{ $practice['postcode'] }}</div>
  	<div class="info">Provider: {{ $practice['provider'] }} T: {{ $practice['phone'] }} F: {{ $practice['fax'] }}</div>
  </div>
     <p>{!! $letters !!}</p>
  </div>
</body>
</body>
</html>