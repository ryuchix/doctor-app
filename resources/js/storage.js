import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
import createPersistedState from 'vuex-persistedstate';

export const store = new Vuex.Store({
  state: {
    access_token: localStorage.getItem("access_token") || ''
  },
  plugins: [createPersistedState()],
  mutations: {
    add: state => state.access_token
  }
});