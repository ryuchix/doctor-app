window.Vue = require('vue');
import '../sass/app.scss'

import 'bootstrap'

import router from "./router";
import App from "./Components/App";

import VueHtmlToPaper from 'vue-html-to-paper';
const options = {
  name: '_blank',
  specs: [
    'fullscreen=yes',
    'titlebar=yes',
    'scrollbars=yes'
  ],
  styles: [
    '../css/app.css',
  ]
}
 
Vue.use(VueHtmlToPaper, options);

import VModal from 'vue-js-modal'
Vue.use(VModal, { dynamic: true, dynamicDefaults: { clickToClose: false } })

var VueCookie = require('vue-cookie');
// Tell Vue to use the plugin
Vue.use(VueCookie);

import VueMask from 'v-mask'
Vue.use(VueMask);

Vue.use(require('vue-moment'));

const app = new Vue({
    el: '#app',
    router,
    render: (h) => h(App)
});