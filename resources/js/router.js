import Vue from "vue";
import VueRouter from "vue-router";

import LoginComponent from "./AuthComponent/Login";
import HomeComponent from "./AuthComponent/Home";
import RegisterComponent from "./AuthComponent/Register";
import DashboardComponent from "./DashboardComponent/Dashboard";
import UserComponent from "./UserComponent/User";
import axios from 'axios';
import VueAxios from 'vue-axios';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: "/",
            name: "home",
            component: HomeComponent,
            meta: {
                auth: true
            }
        },
        {
            path: "/dashboard/:letterid",
            name: "dashboard",
            component: DashboardComponent,
            meta: {
                auth: true
            }
        },
        {
            path: "/user",
            name: "user",
            component: UserComponent,
            meta: {
                auth: true
            }
        },
        {
            path: "/login",
            name: "login",
            component: LoginComponent,
            meta: {
                auth: false
            }
        },
        {
            path: "/register",
            name: "register",
            component: RegisterComponent,
            meta: {
                auth: false
            }
        },
    ]
});

export default router;