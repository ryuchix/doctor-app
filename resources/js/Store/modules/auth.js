/* eslint-disable promise/param-names */
import {
  AUTH_REQUEST,
  AUTH_ERROR,
  AUTH_SUCCESS,
  AUTH_LOGOUT
} from "../actions/auth";
import { USER_REQUEST } from "../actions/user";
import apiCall from "../../Utils/api";
import Constant,{ constant } from '../../DashboardComponent/Constant';
import axios from 'axios';

const state = {
  token: localStorage.getItem("access_token") || "",
  status: "",
  hasLoadedOnce: false
};

const getters = {
  isAuthenticated: state => !!state.token,
  authStatus: state => state.status
};

const actions = {
  [AUTH_REQUEST]: ({commit, dispatch}, user) => {
    return new Promise((resolve, reject) => { // The Promise used for router redirect in login
      commit(AUTH_REQUEST)
      axios({url: constant.loginUrl, data: user, method: 'POST' })
        .then(resp => {
          const token = resp.data.access_token
          sessionStorage.setItem('access_token', token) // store the token in localstorage
          axios.defaults.headers.common['Authorization'] = token
          commit(AUTH_SUCCESS, token)
          // you have your token, now log in your user :)
          dispatch(USER_REQUEST)
          resolve(resp)
        })
      .catch(err => {
        commit(AUTH_ERROR, err)
        localStorage.removeItem('access_token') // if the request fails, remove any possible user token if possible
        reject(err)
      })
    })
  }
}

const mutations = {
  [AUTH_REQUEST]: state => {
    state.status = "loading";
  },
  [AUTH_SUCCESS]: (state, resp) => {
    state.status = "success";
    state.token = resp.token;
    state.hasLoadedOnce = true;
  },
  [AUTH_ERROR]: state => {
    state.status = "error";
    state.hasLoadedOnce = true;
  },
  [AUTH_LOGOUT]: state => {
    state.token = "";
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};